@extends('layouts.master')
@section('title', 'Home')
@section('content')
    <table border="1">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        @foreach($allData as $value)

        <tr>
            <td>{!! $value->name !!}</td>
            <td>{!! $value->description !!}</td>
            <td>
                <a href="{{url('user/edit', $value->id)}}">Edit</a>

            </td>
        </tr>
        @endforeach
    </table>
@stop

